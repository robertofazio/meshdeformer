/* 
 * Classe per la ricezione dell'OSC
 */

using UnityEngine;
using System.Collections;

public class OSCReceiver : MonoBehaviour {
	
	public string			RemoteIP 				= "127.0.0.1";
	public int 				SendToPort 				= 57131;
	public int 				ListenerPort 			= 12345;
	
	private Osc 			handler;
	private UDPPacketIO udp;
	
	public Mover myMover;
	
	void Start () 
	{		
		udp = gameObject.AddComponent("UDPPacketIO") as UDPPacketIO;
		udp.init(RemoteIP, SendToPort, ListenerPort);
		handler = gameObject.AddComponent("Osc") as Osc;
		handler.init(udp);
		
		handler.SetAddressHandler("/alti/", ListenEvent);
		handler.SetAddressHandler("/medi/", ListenEvent);
		handler.SetAddressHandler("/bassi/", ListenEvent);
		
	}
	
	void Update () {
		
	}
	
	public void ListenEvent(OscMessage oscMessage)
	{	
		string address = oscMessage.Address;
		
		Debug.Log( address );
		if(address == "/alti/")
		{
			float newValue = (float)System.Convert.ToSingle(oscMessage.Values[0]);
			myMover.displacement = new Vector3( myMover.displacement.x, newValue/50f, myMover.displacement.z );
		}
		
		
		if(address == "/medi/")
		{
			float newValue = (float)System.Convert.ToSingle(oscMessage.Values[0]);
			myMover.displacement = new Vector3( newValue/50f, myMover.displacement.y, myMover.displacement.z );
		}
		
		if(address == "/bassi/")
		{
			float newValue = (float)System.Convert.ToSingle(oscMessage.Values[0]);
			myMover.displacement = new Vector3( myMover.displacement.x, myMover.displacement.y,newValue/50f );
		}
		
		
	} 
}