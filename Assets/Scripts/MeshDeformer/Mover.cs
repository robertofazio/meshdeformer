﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour 
{
	
	public float 	speed;
	public Vector3 	displacement;
	
	Vector3 startingPos;
	// Use this for initialization
	void Start () 
	{
		startingPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = startingPos + displacement * Mathf.Sin( Time.time * speed );
	}
}